import { defineStore } from 'pinia'

export const useSearchStore = defineStore('search', {
  state: () => {
    return { 
      image: null,
      tags: null
    }
  },
  actions: {
    setImage(image) {
      this.image = image
    },
    setTags(tags) {
      this.tags = tags
    }
  },
  getters: {
    isSearchByImage () {
      return !!this.image
    }
  }
})