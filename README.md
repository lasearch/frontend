## Setup

Make sure to install the dependencies:

```bash
# npm
npm install
```

## Development Server

Start the development server on http://localhost:3000

```bash
npm run dev
```

## Production

Build the application for production:

```bash
npm run build
```

Locally preview production build:

```bash
npm run preview
```

## Deploy docker container
1. Set backend url in API_BASE_URL at /api/index.js 

2. Build image
```bash
docker build -t lizasearch-front .
```

3. Run container
```bash
docker run -t lizasearch-front -d -p 3000:3000
```

If you have any questions, dm me in telegram: https://t.me/yaroslav_yudi
