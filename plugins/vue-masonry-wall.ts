import MasonryWall from '@yeger/vue-masonry-wall'

export default defineNuxtPlugin((nuxtApp) => {
  if (typeof global !== 'undefined') {
    global.ResizeObserver = class ResizeObserver {
      public constructor() {}
      public disconnect() {}
      public observe() {}
      public unobserve() {}
    }
  }
  nuxtApp.vueApp.use(MasonryWall)
})