FROM node:16-alpine as builder

WORKDIR /app

RUN apk add --no-cache git python3

COPY package.json \
     package-lock.json ./
RUN npm ci

COPY . .
RUN npm run build


FROM node:16-alpine

WORKDIR /app

COPY --from=builder /app/.nuxt/ .nuxt/
COPY --from=builder /app/.output/ .output/
COPY --from=builder /app/node_modules/ node_modules/
COPY . .

EXPOSE $PORT

CMD npm run preview
