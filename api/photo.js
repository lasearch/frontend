import { api } from ".";

export function uploadImages(files, uploadProgressCallback) {
  const formData = new FormData();
  for (const file of files) {
    console.log('file', file)
    formData.append("photos", file, file.name);
  }
  return api.post("/photos/upload/batch", formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
    onUploadProgress: uploadProgressCallback,
  });
}

export function uploadSubmit (data) {
    return api.post(`/photos/submit/batch`, data)
}

export function editPhoto (id, tags) {
  return api.put(`/photos/${id}`, { tags })
}