import { api } from ".";

export function fetchTags () {
    return api.get('/tags')
}
