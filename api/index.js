import axios from "axios";

const API_BASE_URL = 'http://51.250.19.21:8000'

function getToken() {
    return 'FAKE-TOKEN'
}

const api = axios.create({
  baseURL: API_BASE_URL + '/api',
  timeout: 600000,
});

api.interceptors.request.use((request) => {
  const token = getToken();
  if (token) {
    request.headers.Authorization = `Bearer ${token}`;
  }
  return request;
});


export { api };
