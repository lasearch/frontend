import { api } from ".";

export function search(tags, page = 1, limit = 20) {
  const tagsParam = tags.map((el) => `tags=${el}`).join("&");
  return api.get(`/search/tags?${tagsParam}&page=${page}&limit=${limit}`);
}

export function searchByImage(file, page = 1, limit = 50) {
  const formData = new FormData();
  formData.append("photo", file);
  return api.post(`/search/photo?page=${page}&limit=${limit}`, formData);
}

export function rearrange(images, tags, page = 1, limit = 20) {
  return api.post(`/search/rearange?page=${page}&limit=${limit}`, { ids: images, tags } );
}
