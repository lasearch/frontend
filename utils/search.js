export const convertGroups = (items) => {
    const result = [];
    for (const item of items) {
      if (item.length > 0) {
        const lastIndex = Math.min(item.length, 4)
        result.push({
          photo_url: item[0].photo_url,
          tags: item[0].tags,
          id: item[0].id,
          children: item.slice(1, lastIndex),
        });
      } else {
        result.push(item);
      }
    }
    return result
  };