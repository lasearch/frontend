import { defineNuxtConfig } from 'nuxt'

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  ssr: false,
  css: [ 'vuetify/lib/styles/main.sass', 'mdi/css/materialdesignicons.min.css', '@/assets/css/styles.css'],
  build: {
    transpile: ['vuetify'],
    postcss: {
      postcssOptions: {
        plugins: {
          tailwindcss: {},
          autoprefixer: {},
        },
      },
    },
  },
  buildModules: [
    '@pinia/nuxt',
  ],
  vite: {
    define: {
      'process.env.DEBUG': false,
    },
  },
  env: {
    API_URL: 'http://51.250.19.21:8000/'
  }
})
